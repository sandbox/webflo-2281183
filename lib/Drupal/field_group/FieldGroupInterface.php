<?php
/**
 * @file
 * Contains \Drupal\field_group\FieldGroupInterface.
 */

namespace Drupal\field_group;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a field_group data object.
 */
interface FieldGroupInterface extends ConfigEntityInterface {
  // Any public methods your class has should be here as well.
}
